# [JavaScript Primer](https://jsprimer.net) の質疑応答
テキストを読んでいて分からないところを質問してください。

**ここは公開されています。**


## 質問方法
* 質問は [New issue](https://gitlab.com/carrier-design-2019/jsprimer-question/issues/new) に投稿してください
* `Title` は分かりやすい要約を書いてください (`分かりません` や `質問です` が並ぶとどれがどれかわからなくなります)
* `Description` には質問の内容を詳しく書きます

書き方は[技術系メーリングリストで質問するときのパターン・ランゲージ](https://www.hyuki.com/writing/techask.html)を参考にするとよいでしょう。

ソースコードは [Repl.it](https://repl.it) に置いて、リンクを張る方法もあります。[Repl.it](https://repl.it) の右上のメニューから `my repls` -> `new repl` をクリックし，HTML か JavaScript を選んでください。